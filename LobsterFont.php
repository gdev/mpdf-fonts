<?php

namespace Gdev\Fonts;

class LobsterFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Lobster';
    }

    public function getFontData()
    {
        return [
            'R' => 'Lobster-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
        return "https://fonts.googleapis.com/css?family=Lobster";
    }
}
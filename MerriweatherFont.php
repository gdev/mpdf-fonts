<?php

namespace Gdev\Fonts;

class MerriweatherFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Merriweather';
    }

    public function getFontData()
    {
        return [
            'R' => 'Merriweather-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
        return "https://fonts.googleapis.com/css?family=Merriweather";
    }
}
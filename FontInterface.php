<?php

namespace Gdev\Fonts;

interface FontInterface {

    public function getDirectory();
    public function getFontData();
    public function getStylesheetLink();

}
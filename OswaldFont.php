<?php

namespace Gdev\Fonts;

class OswaldFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Oswald';
    }

    public function getFontData()
    {
        return [
            'R' => 'Oswald-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
        return "https://fonts.googleapis.com/css?family=Oswald";
    }
}
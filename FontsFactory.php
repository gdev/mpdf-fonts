<?php

namespace Gdev\Fonts;

class FontsFactory
{

    /**
     * @param string $font
     * @return FontInterface
     * @throws \Exception
     */
    public static function Make($font) {

        $fontClass = "\\Gdev\\Fonts\\" . $font . "Font";

        if(!class_exists($fontClass)) {
            throw new \Exception("Font $font does not exist");
        }
        $fontClass = new $fontClass();

        if(!($fontClass instanceof FontInterface)) {
            throw new \Exception("Font $font does not implement FontInterface");
        }

        return $fontClass;

    }

}
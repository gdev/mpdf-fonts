<?php

namespace Gdev\Fonts;

class RobotoFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Roboto';
    }

    public function getFontData()
    {
        return [
            'R' => 'Roboto-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
       return "https://fonts.googleapis.com/css?family=Roboto";
    }
}
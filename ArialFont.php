<?php

namespace Gdev\Fonts;

class ArialFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Arial';
    }

    public function getFontData()
    {
        return [
            'R' => 'Arial.ttf'
        ];
    }

    public function getStylesheetLink()
    {
       return "";
    }
}
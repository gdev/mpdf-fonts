<?php

namespace Gdev\Fonts;

class SarabunFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Sarabun';
    }

    public function getFontData()
    {
        return [
            'R' => 'Sarabun-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
        return "https://fonts.googleapis.com/css?family=Sarabun";
    }
}
<?php

namespace Gdev\Fonts;

class LatoFont implements FontInterface
{

    public function getDirectory()
    {
        return __DIR__ . '/static/Lato';
    }

    public function getFontData()
    {
        return [
            'R' => 'Lato-Regular.ttf'
        ];
    }

    public function getStylesheetLink()
    {
       return "https://fonts.googleapis.com/css?family=Lato";
    }
}